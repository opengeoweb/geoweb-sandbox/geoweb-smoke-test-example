import { defineConfig } from "cypress";

export default defineConfig({
  viewportHeight: 1080,
  viewportWidth: 1920,
  chromeWebSecurity: false,
  defaultCommandTimeout: 30000,
  requestTimeout: 30000,
  env: {
    app_url: "https://example.opengeoweb.com/",
    username: "example.user",
    password: "",
    version: "master",
    api_key: "tst",
  },
  retries: {
    // Configure retry attempts for `cypress run`
    runMode: 1,
    // Configure retry attempts for `cypress open`
    openMode: 0,
  },
  e2e: {
    setupNodeEvents(on, config) {
      require("cypress-log-to-output").install(on, (type, event) => {
        if (event.level === "error" || event.type === "error") {
          return false; // set to true to enable more error logging in the pipeline
        }
        return false;
      });
    },
    supportFile: "./cypress/support/commands.ts",
  },
  video: false,
});
