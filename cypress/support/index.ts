declare namespace Cypress {
    interface Chainable {
      /**
       * Check the test settings inside cypress.json, will fail the test if something is missing.
       * @example cy.checkTestSettings()
       */
      checkTestSettings(): void;
      /**
       * Login with the credentials set in cypress.json.
       * @example cy.login()
       */
      login(): void;
      /**
       * Logs out.
       * @example cy.logout()
       */
      logout(): void;
      /**
       * Verifies the version displayed in the user menu with the one in cypress.json
       * @example cy.checkVersion()
       */
      checkVersion(): void;
    }
  }
  