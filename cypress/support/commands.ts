import "@testing-library/cypress/add-commands";

Cypress.Commands.add("checkTestSettings", () => {
  const { username, password, app_url, version } = Cypress.env();
  expect(username, "username was set").to.be.a("string").and.not.be.empty;
  expect(app_url, "app_url was set").to.be.a("string").and.not.be.empty;
  expect(version, "version was set").to.be.a("string").and.not.be.empty;
  if (typeof password !== "string" || !password) {
    throw new Error(
      "Missing password value, set your login credentials in cypress.json"
    );
  }
});

Cypress.Commands.add("login", () => {
  const { app_url, username, password } = Cypress.env();

  cy.intercept("GET", /oauth2\/authorize/).as("get-authorize");
  cy.intercept("GET", /\/login\?client*/).as("get-login");
  cy.intercept("GET", /\/code\?code=/).as("get-code");
  cy.intercept("POST", /\/infra\/auth\/token/).as("post-token");
  cy.clearAllSessionStorage({ log: true });
  cy.visit(app_url);
  cy.wait("@get-authorize").then((interception) => {
    const response = interception.response;
    expect(response.statusCode).to.equal(302);

    // only login if redirected to login page
    if (
      response.headers.location.includes(".amazoncognito.com/login?client_id=")
    ) {
      cy.log("redirected to login page");

      cy.wait("@get-login").then((interception) => {
        const cookie = interception.request.headers["cookie"];
        expect(cookie).contains("XSRF-TOKEN");
        cy.intercept("POST", /\/login\?cli*/, (req) => {
          req.headers["cookie"] = cookie;
        }).as("post-login");

        cy.get(".visible-lg").within(() => {
          cy.get("#signInFormUsername")
            .type(username)
            .should("have.value", username);
          cy.get("#signInFormPassword")
            .type(password, { log: false })
            .should(($el) => {
              if ($el.val() !== password) {
                throw new Error("Different value of typed password");
              }
            });
          cy.get("[name=signInSubmitButton]").click();
          // wait for the login to succeed
          cy.wait("@post-login").then((interception) => {
            expect(interception.request.headers["cookie"]).to.equal(cookie);
            expect(interception.response.headers.location).not.to.contain(
              "error"
            );
          });
        });
      });
    } else {
      cy.log("already logged in");
    }

    // wait for the code and token to return
    cy.wait("@get-code").its("response.statusCode").should("eq", 200);
    cy.wait("@post-token").then((interception) => {
      expect(interception.response.statusCode).to.equal(200);

      const request = interception.request.body;
      cy.log("token request:", request);
      expect(request).property("code").to.exist;
      expect(request).property("code_verifier").to.not.be.null;

      const response = interception.response.body.body;
      cy.log("token response:", response);
      expect(response).property("access_token");
      expect(response).property("id_token");
      expect(response).property("refresh_token");
    });

    // wait for the homepage to load
    cy.url().should("equal", app_url);
  });
});

Cypress.Commands.add("logout", () => {
  cy.intercept("GET", /\/logout/).as("get-logout");
  cy.findByTestId("userButton").click({ force: true });
  cy.findByText("Logout").click({ force: true });
  cy.findByTestId("userButton").should("not.exist");
  cy.wait("@get-logout").its("response.statusCode").should("eq", 302);
  cy.url().should("contain", "login");
});

Cypress.Commands.add("checkVersion", () => {
  const { version, app_url } = Cypress.env();
  cy.findByTestId("userButton").click();
  cy.findByTestId("be-version-menu").should("contain.text", version);

  // check app title displayed in the header
  // retrieve value from config.json
  cy.request({
    url: `${app_url}assets/config.json`,
  }).then((response) => {
    expect(response.status).to.eq(200);
    cy.findAllByText(response.body.GW_FEATURE_APP_TITLE).last().click();
  });
});

Cypress.on("uncaught:exception", (err, runnable) => {
  return false;
});

before(() => {
  cy.checkTestSettings();
});
