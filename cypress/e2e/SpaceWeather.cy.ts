beforeEach(() => {
  cy.login();
});

afterEach(() => {
  cy.logout();
});

const makeNotification = (listLength: number) => {
  cy.findByTestId("notifications-newnotification").click();
  cy.findByTestId("eventlevel-select").click();
  cy.findByText("R1").click();
  cy.findByText("M1").should("exist");
  cy.findByText("0.00001").should("exist");
  cy.findByTestId("datasource-input")
    .type("DATA SOURCE")
    .should("have.value", "DATA SOURCE");
  cy.findByTestId("notification-text")
    .type("smoke test notification")
    .should("have.value", "smoke test notification");
  cy.findByTestId("notification-title")
    .type("smoke test notification title")
    .should("have.value", "smoke test notification title");
  cy.findByTestId("notification-repopulate").click();
  cy.findByTestId("notification-title").should(
    "have.value",
    "KNMI ruimteweer: radio blackout alarm door zonnevlam (R1)"
  );
  cy.findByTestId("notification-text").should(
    "include.text",
    "is de start van een sterke zonnevlam gedetecteerd in de metingen van Röntgenstraling van de Zon door de DATA SOURCE satelliet."
  );

  cy.findByTestId("issue").click();

  // check that the new notification dialog is closed
  cy.findByTestId("issue").should("not.exist");

  // check that a new notification is added to the list
  cy.findAllByTestId("notificationRow-listitem").should(
    "have.length",
    listLength + 1
  );
};

describe("SpaceWeather Smoke Test", () => {
  it("should create a new notification", () => {
    // close the workspace dialog
    cy.findByTestId('workspaceMenuBackdrop').findByTestId('closeBtn').click();
    // make a spaceweather notification
    cy.findByTestId("menuButton").click();
    cy.findByText("Space Weather Forecast").click();

    cy.findByTestId("notificationList-skeleton").should("exist");
    cy.findByTestId("notificationList-skeleton").should("not.exist");

    cy.get("body").then(($body) => {
      if ($body.find('[data-testid="notificationRow-listitem"]').length) {
        // get length of list to verify later
        cy.findAllByTestId("notificationRow-listitem").then(($elements) => {
          const listLength = $elements.length;
          makeNotification(listLength);
        });
      } else {
        // list is empty
        const listLength = 0;
        makeNotification(listLength);
      }
    });

    cy.findByTestId("SpaceWeatherModule").findByTestId("closeBtn").click();
    cy.findByTestId("SpaceWeatherModule").should("not.exist");
  });
});
