beforeEach(() => {
  cy.login();
});

afterEach(() => {
  cy.logout();
});

describe("Warnings Smoke Test", () => {
  it("should save a new object, update and delete it", () => {
    const now = new Date().toUTCString();

    cy.intercept("POST", "/drawings/").as("saveNewObject");
    cy.intercept("POST", /\/drawings\/.*\//).as("updateObject");
    cy.intercept("POST", /\/drawings\/.*?delete=true/).as("deleteObject");

    // close the workspace dialog
    cy.findByTestId("workspaceMenuBackdrop").findByTestId("closeBtn").click();
    // close layer manager
    cy.findByLabelText("Layer Manager").click();

    // open drawing toolbox
    cy.findByLabelText("Drawing Toolbox").click();
    cy.findByText("Drawing Toolbox 1_screen").should("be.visible");
    // draw a polygon
    cy.findByLabelText("Polygon").click();
    cy.findByTestId("ConfigurableMap").click(650, 400);
    cy.findByTestId("ConfigurableMap").click(660, 450);
    cy.findByTestId("ConfigurableMap").click(500, 500);
    cy.findByTestId("drawtools-polygon").type("{esc}");
    // Change object name
    cy.findByRole("textbox", { name: "Object name" }).clear();
    cy.findByRole("textbox", { name: "Object name" }).type(`New object ${now}`);
    // save object
    cy.findByText("Save new object").click();
    cy.wait("@saveNewObject");
    // button text should be updated
    cy.findByText("Update object").should("be.visible");
    // snackbar should be visible
    cy.findByText(`Object New object ${now} has been saved!`).should(
      "be.visible"
    );

    // open object manager
    cy.findByLabelText("Object Manager Button for 1_screen").click();
    // click edit button for saved object
    cy.findByText(`New object ${now}`)
      .parentsUntil("ul")
      .last()
      .findByRole("button", { name: "Object options" })
      .click();
    cy.findByText("Edit").click();
    // object manager should close
    cy.findAllByLabelText("Object options").should("have.length", 0);
    // drawing toolbox should open for selected object
    cy.findByText("Drawing Toolbox 1_screen").should("be.visible");
    cy.findByRole("textbox", { name: "Object name" }).should(
      "have.value",
      `New object ${now}`
    );
    // Change object name
    cy.findByRole("textbox", { name: "Object name" }).clear();
    cy.findByRole("textbox", { name: "Object name" }).type(
      `Update object ${now}`
    );
    // save
    cy.findByText("Update object").click();
    cy.wait("@updateObject");
    // snackbar should be visible
    cy.findByText(`Object Update object ${now} has been updated!`).should(
      "be.visible"
    );

    // open object manager again
    cy.findByLabelText("Object Manager Button for 1_screen").click();
    // click delete button for saved object
    cy.findByText(`Update object ${now}`)
      .parentsUntil("ul")
      .last()
      .findByRole("button", { name: "Object options" })
      .click();
    cy.findByText("Delete").click();
    // confirm
    cy.findByText("Delete").click();
    cy.wait("@deleteObject");
    // snackbar should be visible
    cy.findByText(`Update object ${now} has been deleted`).should("be.visible");
    // list should be updated
    cy.findByText(`Update object ${now}`).should("not.exist");
  });
});
