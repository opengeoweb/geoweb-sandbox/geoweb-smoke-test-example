beforeEach(() => {
  cy.login();
});

afterEach(() => {
  cy.logout();
});

describe("WorkspacePresets Smoke Test", () => {
  const { app_url } = Cypress.env();

  it("it should show a workspace menu with workspace list", () => {
    cy.intercept("GET", "/workspacepreset?scope=user,system").as(
      "getWorkspace"
    );
    // verify that the workspace menu is opened & list has a length greater than 0
    cy.findByText("Workspace menu").should("be.visible");
    cy.wait("@getWorkspace");
    cy.findAllByTestId("workspace-selectListRow").should("have.length.gt", 0);
  });

  it("it should duplicate a workspace and delete the created workspace", () => {
    const now = new Date().toUTCString();
    cy.intercept("GET", "/workspacepreset?scope=user,system").as(
      "getWorkspace"
    );
    cy.intercept("GET", "/workspacepreset?scope=user,system&search=Empty").as(
      "getWorkspaceEmpty"
    );
    cy.intercept("POST", "/workspacepreset").as("postWorkspace");
    cy.intercept("DELETE", /\/workspacepreset/).as("deleteWorkspace");

    // verify workspace menu is visible
    cy.findByText("Workspace menu").should("be.visible");
    cy.wait("@getWorkspace").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // Now filter for Empty map in the virtual list
    cy.findAllByTestId("workspace-selectlist-connect")
      .find("input")
      .type("Empty");
    cy.wait("@getWorkspaceEmpty").then((interception) => {
      // check the list length, duplicate a workspace and verify that
      // the list length has increased with +1
      const initialResult = interception.response.body;
      cy.findByText(`${initialResult.length} results`);

      cy.findAllByTestId("workspace-listOptionsButton").first().click();
      cy.findByText("Duplicate").click();
      cy.get("input[name='title']").clear().type(`Test dupl. EmptyMap ${now}`);
      cy.findByText("Save").click();
      cy.wait("@postWorkspace");
      cy.wait("@getWorkspaceEmpty").then((interception) => {
        const result = interception.response.body;
        cy.findByText(`${result.length} results`);
        // make sure list length has increased by 1
        cy.wrap(result).should("have.length", initialResult.length + 1);
      });

      // find the duplicated workspace and delete
      cy.findByLabelText(`Test dupl. EmptyMap ${now}`)
        .next()
        .findByTestId("workspace-listDeleteButton")
        .click();
      cy.findByText("Delete").click();
      cy.wait("@deleteWorkspace");
      cy.wait("@getWorkspaceEmpty").then((interception) => {
        const result = interception.response.body;
        cy.findByText(`${result.length} results`);
        // make sure list length has decreased by 1
        cy.wrap(result).should("have.length", initialResult.length);
      });
    });
  });

  it("it should duplicate, edit, save and delete a workspace", () => {
    const now = new Date().toUTCString();
    cy.intercept("GET", "/workspacepreset?scope=user,system&search=Empty").as(
      "getWorkspaceEmpty"
    );
    cy.intercept("POST", "/workspacepreset").as("postWorkspace");
    cy.intercept("PUT", /\/workspacepreset/).as("editWorkspace");
    cy.intercept("DELETE", /\/workspacepreset/).as("deleteWorkspace");
    cy.intercept("GET", "/viewpreset").as("getViewpreset");
    cy.intercept("GET", "/viewpreset?search=Empty").as("getViewpresetEmpty");
    cy.intercept("GET", "/viewpreset?search=Satellite").as("getViewpresetSat");

    // verify workspace menu is visible
    cy.findByText("Workspace menu").should("be.visible");

    // statement that makes sure workspace menu has loaded
    // by default it will show "New workspace" (length = 1)
    // this makes sure more workspaces in the list have been loaded
    cy.findAllByTestId("workspace-selectListRow").should(
      "have.length.greaterThan",
      1
    );

    // Now filter for Empty map in the virtual list
    cy.findAllByTestId("workspace-selectlist-connect")
      .find("input")
      .type("Empty");
    cy.wait("@getWorkspaceEmpty").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // duplicate a workspace
    cy.findAllByTestId("workspace-listOptionsButton").first().click();
    cy.findByText("Duplicate").click();
    cy.get("input[name='title']").clear().type(`Test edit EmptyMap ${now}`);
    cy.findByText("Save").click();
    cy.wait("@postWorkspace");
    cy.wait("@getWorkspaceEmpty").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // find the duplicated workspace and open
    cy.contains(`Test edit EmptyMap ${now}`).click();
    cy.findByTestId("workspace-options-toolbutton").should(
      "contain",
      "Test edit"
    );
    cy.findAllByText("Empty Map").should("be.visible");

    // test Save as
    cy.findByTestId("open-viewpresets").click();
    cy.findByTestId("viewpresetMenuBackdrop").find("input").type("Empty");
    cy.wait("@getViewpresetEmpty").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    cy.findAllByText("Empty").last().click();
    cy.findByTestId("workspace-options-toolbutton").click();
    cy.findByText("Save as").click();
    cy.get("input[name='title']").clear().type(`Test save as Empty ${now}`);
    cy.findByText("Save").click();
    cy.wait("@postWorkspace");
    cy.findByTestId("workspace-options-toolbutton").should(
      "contain",
      "Test save as"
    );

    // edit and Save the workspace
    cy.findByTestId("layerManagerButton").click();
    cy.findByTestId("open-viewpresets").click();
    cy.findByText("View presets menu").should("exist");
    cy.findAllByTestId("viewpreset-selectListRow").should(
      "have.length.greaterThan",
      0
    );

    // Make sure the Satellite viewpreset comes in view
    cy.findByTestId("viewpresetMenuBackdrop").find("input").clear();
    cy.wait("@getViewpreset").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });
    cy.findByTestId("viewpresetMenuBackdrop").find("input").type("Satellite");
    cy.wait("@getViewpresetSat").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    cy.findByTestId("viewpreset-selectList")
      .findAllByText("Satellite")
      .last()
      .click();
    cy.findByTestId("workspace-options-toolbutton").click();
    cy.findByText("Save").click();
    cy.wait("@editWorkspace");
    cy.findByTestId("workspace-options-toolbutton").should(
      "contain",
      "Test save as"
    );

    // cleanup the created workspaces

    // delete the workspace from the top bar
    cy.findByTestId("workspace-options-toolbutton").click();
    cy.findByText("Delete").click();
    // confirm to delete
    cy.findByText("Delete").click();
    cy.wait("@deleteWorkspace");
    // check that you return to the "New workspace"
    cy.findByTestId("workspace-options-toolbutton").should(
      "contain",
      "New workspace"
    );

    // open menu
    cy.findByTestId("workspaceMenuButton").click();
    cy.findByText("Workspace menu").should("be.visible");

    // Filter for the created workspace
    cy.findAllByTestId("workspace-selectlist-connect")
      .find("input")
      .type(`Test edit EmptyMap ${now}`);

    // find the edit workspace and delete
    cy.findByLabelText(`Test edit EmptyMap ${now}`)
      .next()
      .findByTestId("workspace-listDeleteButton")
      .click();
    cy.findByText("Delete").click();
    cy.wait("@deleteWorkspace");
  });

  it("it should stay on the same workspace if you refresh the page", () => {
    cy.intercept("GET", "/workspacepreset?scope=user,system").as(
      "getWorkspace"
    );
    cy.intercept("GET", "/workspacepreset?scope=user,system&search=Empty").as(
      "getWorkspaceEmpty"
    );

    // verify that the workspace menu is open
    cy.findByText("Workspace menu").should("be.visible");

    cy.findAllByTestId("workspace-selectlist-connect")
      .find("input")
      .type("Empty");
    cy.wait("@getWorkspaceEmpty").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // open a workspace
    cy.findAllByText("Empty").first().click();
    cy.wait("@getWorkspaceEmpty").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // refresh the page and verify you remain on the same workspace
    cy.url().then((url) => {
      cy.reload();
      cy.wait("@getWorkspace");
      cy.url().should("eq", url);
      cy.findByTestId("workspace-options-toolbutton").should(
        "contain",
        "Empty"
      );
    });
  });

  it("it should go back to 'New workspace' if you click the GeoWeb logo", () => {
    cy.intercept("GET", "/workspacepreset?scope=user,system&search=Empty").as(
      "getWorkspaceEmpty"
    );

    // verify that the workspace menu is open
    cy.findByText("Workspace menu").should("be.visible");

    cy.findAllByTestId("workspace-selectlist-connect")
      .find("input")
      .type("Empty");
    cy.wait("@getWorkspaceEmpty").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // open a workspace
    cy.findAllByText("Empty").first().click();
    cy.wait("@getWorkspaceEmpty").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // click on the geoweb logo and verify that you return to "New workspace"
    // check app title displayed in the header
    // retrieve value from config.json
    cy.request({
      url: `${app_url}assets/config.json`,
    }).then((response) => {
      expect(response.status).to.eq(200);
      cy.findAllByText(response.body.GW_FEATURE_APP_TITLE).last().click();
    });
    cy.wait("@getWorkspaceEmpty").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });
    cy.findByText("Workspace menu").should("be.visible");
    cy.findByTestId("workspace-options-toolbutton").should(
      "contain",
      "New workspace"
    );
  });

  it("it should filter workspacepresets when clicking on filter chips", () => {
    const now = new Date().toUTCString();
    cy.intercept("GET", "/workspacepreset?scope=user,system&search=Empty").as(
      "getWorkspaceEmpty"
    );
    cy.intercept("POST", "/workspacepreset").as("postWorkspace");
    cy.intercept("GET", "/workspacepreset?scope=user&search=Empty").as(
      "getMyWorkspacesEmpty"
    );
    cy.intercept("GET", "/workspacepreset?scope=system&search=Empty").as(
      "getSystemWorkspacesEmpty"
    );
    cy.intercept("DELETE", /\/workspacepreset/).as("deleteWorkspace");

    // verify that the workspace menu is open
    cy.findByText("Workspace menu").should("be.visible");

    cy.findAllByTestId("workspace-selectlist-connect")
      .find("input")
      .type("Empty");
    cy.wait("@getWorkspaceEmpty").then((interception) => {
      const initialResult = interception.response.body;
      cy.findByText(`${initialResult.length} results`);
      cy.wrap(initialResult).should("have.length.above", 0);

      // duplicate a workspace (In this case Empty Map) to create a user workspace
      cy.findAllByTestId("workspace-listOptionsButton").first().click();
      cy.findByText("Duplicate").click();
      cy.get("input[name='title']").clear().type(`Filt. Empty map ${now}`);
      cy.findByText("Save").click();
      cy.wait("@postWorkspace");
      cy.wait("@getWorkspaceEmpty").then((interception) => {
        const result = interception.response.body;
        cy.findByText(`${result.length} results`);
        cy.wrap(result).should("have.length", initialResult.length + 1);
      });
    });

    // open & close the workspace menu
    cy.findByTestId("workspaceMenuButton").click();
    cy.findByText("Workspace menu").should("not.be.visible");
    cy.findByTestId("workspaceMenuButton").click();
    // verify that the workspace menu is open
    cy.findByText("Workspace menu").should("be.visible");
    cy.findAllByTestId("workspace-selectListRow").should(
      "have.length.least",
      1
    );

    // test the filter chips
    cy.findAllByTestId("workspace-selectListRow").then((listFilter) => {
      // filter All presets
      cy.findAllByText("All").first().click();
      // list length should remain the same
      cy.findAllByTestId("workspace-selectListRow").then((listAll) => {
        cy.wrap(listAll).should("have.length", listFilter.length);
      });

      // filter My presets
      cy.findAllByText("My presets").first().click();
      cy.wait("@getMyWorkspacesEmpty").then((interception) => {
        const result = interception.response.body;
        cy.findByText(`${result.length} results`);
      });
      cy.findAllByTestId("workspace-selectListRow").should(
        "have.length.greaterThan",
        0
      );
      // filter All presets again
      cy.findAllByText("All").first().click();
      cy.wait("@getWorkspaceEmpty").then((interception) => {
        const result = interception.response.body;
        cy.findByText(`${result.length} results`);
      });
      cy.findAllByTestId("workspace-selectListRow").should(
        "have.length.greaterThan",
        1
      );
      // filter System presets
      cy.findAllByText("System presets").first().click();
      cy.wait("@getSystemWorkspacesEmpty").then((interception) => {
        const result = interception.response.body;
        cy.findByText(`${result.length} results`);
      });
      cy.findAllByTestId("workspace-selectListRow").should(
        "have.length.greaterThan",
        0
      );
    });

    // clean up created workspacepreset
    cy.findAllByText("All").first().click();
    cy.wait("@getWorkspaceEmpty").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });
    cy.findByLabelText(`Filt. Empty map ${now}`)
      .next()
      .findByTestId("workspace-listDeleteButton")
      .click();
    // confirm to delete
    cy.findByText("Delete").click();
    cy.wait("@deleteWorkspace");
  });

  it("it should search using search terms", () => {
    const now = new Date().toUTCString();
    cy.intercept("GET", "/workspacepreset?scope=user,system").as(
      "getWorkspace"
    );
    cy.intercept("GET", "/workspacepreset?scope=system").as(
      "getSystemWorkspaces"
    );
    cy.intercept("GET", "/workspacepreset?scope=user,system&search=Harm").as(
      "getHarmWorkspaces"
    );
    cy.intercept("GET", "/workspacepreset?scope=system&search=harm").as(
      "getHarmSystemWorkspacesLowercase"
    );
    cy.intercept("GET", "/workspacepreset?scope=system&search=HARM").as(
      "getHarmSystemWorkspacesUppercase"
    );
    cy.intercept("GET", "/workspacepreset?scope=system&search=Sat,3h").as(
      "getSat3hWorkspaces"
    );
    cy.intercept("DELETE", /\/workspacepreset/).as("deleteWorkspace");

    // verify that the workspace menu is open
    cy.findByText("Workspace menu").should("be.visible");

    // verify that the list is loaded
    cy.wait("@getWorkspace").then((interception) => {
      const list = interception.response.body;
      cy.findByText(`${list.length} results`);
    });

    // Get all system workspace presets
    cy.findAllByText("System presets").first().click();
    cy.wait("@getSystemWorkspaces").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // Search with lowercase
    cy.get("input").first().type("harm");
    cy.wait("@getHarmSystemWorkspacesLowercase").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // Do same search with all caps
    cy.get("input").first().clear().type("HARM");
    cy.wait("@getHarmSystemWorkspacesUppercase").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // Search by two keywords
    cy.get("input").first().clear().type("Sat 3h");
    cy.wait("@getSat3hWorkspaces").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // clear input
    cy.get("input").first().clear();
    cy.wait("@getSystemWorkspaces").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // Switch back to all
    cy.findAllByText("All").first().click();
    cy.wait("@getWorkspace").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // Duplicate existing preset and assign title/abstract
    cy.findAllByTestId("workspace-listOptionsButton")
      .first()
      .click({ force: true });
    cy.findByText("Duplicate").click();
    cy.get("input[name='title']").clear().type(`New Harmonie ${now}`);
    cy.get("textarea").first().type(`Abstract ${now}`);
    cy.findByText("Save").click();

    // Search new preset
    cy.get("input").first().clear().type(`New Harmonie ${now}`);
    // Check there is only this one
    cy.findByText("1 results");
    // Search for it again, this time based on Abstract
    cy.get("input").first().clear().type(`Abstract ${now}`);
    cy.findByText("1 results");

    // Now, delete the new preset
    cy.findByLabelText(`New Harmonie ${now}`)
      .next()
      .findByTestId("workspace-listDeleteButton")
      .click();
    // confirm to delete
    cy.findByText("Delete").click();
    cy.wait("@deleteWorkspace");
    cy.findByText(`0 results`);
  });
});
