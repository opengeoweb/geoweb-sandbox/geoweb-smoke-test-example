beforeEach(() => {
    cy.login();
  });
  
  afterEach(() => {
    cy.logout();
  });
  
  const makeAirmet = (listLength: number, airmetConfig) => {
    // make a draft AIRMET
    cy.findByTestId("productListCreateButton").click();
    cy.get("#mui-component-select-phenomenon").click();
    cy.findByText("Moderate icing").click();
    cy.findByTestId("type").click();
    cy.findByText("Test").click();
    cy.findByTestId("productform-dialog-draft").click();
    // check that the new AIRMET dialog is closed
    cy.findByTestId("productform-dialog-draft").should("not.exist");
    // check that a new AIRMET is added to the list
    cy.findAllByTestId("productListItem").should("have.length", listLength + 1);
  
    // then update and publish the AIRMET
    cy.findAllByTestId("productListItem").first().click();
    cy.findByTestId("dialogTitle").should(
      "have.text",
      "AIRMET Moderate icing - saved as draft"
    );
  
    // check the displayed firname with name from config
    expect(airmetConfig).to.have.property("fir_areas");
    expect(airmetConfig.fir_areas).to.have.property("EHAA");
    const firName = airmetConfig["fir_areas"]["EHAA"]["fir_name"];
    cy.get("#mui-component-select-locationIndicatorATSR").should(
      "contain",
      `${firName}`
    );
  
    // fill in the form
    cy.get("input[value=OBS]").click();
  
    // enter a point and then delete, check warning message
    cy.findByTestId("drawtools-delete").click();
    cy.findByTestId("MapView").click(500, 500);
    cy.findByTestId("drawtools-point").type("{esc}");
    cy.findByTestId("drawtools-point").click();
    cy.findByText("A start position drawing is required").should("exist");
  
    // enter polygon drawing mode and draw polygon with many intersections
    cy.findByTestId("drawtools-polygon").click();
    cy.findByTestId("MapView").click(650, 400);
    cy.findByTestId("MapView").click(660, 450);
    cy.findByTestId("MapView").click(500, 450);
    cy.findByTestId("MapView").click(500, 200);
    cy.findByTestId("MapView").click(750, 300);
    cy.findByTestId("drawtools-polygon").type("{esc}");
    cy.findByText(
      "Intersection of the drawn polygon with the FIR-boundary has more than 6 individual points. Check if the TAC in the preview corresponds to the intended area."
    ).should("exist");
  
    // erase and check warning again
    cy.findByTestId("drawtools-delete").click();
    cy.findByText("A start position drawing is required").should("exist");
  
    // draw a polygon with too many vertices (more than 6) and check warning
    cy.findByTestId("drawtools-polygon").click();
    cy.findByTestId("MapView").click(650, 400);
    cy.findByTestId("MapView").click(655, 420);
    cy.findByTestId("MapView").click(660, 430);
    cy.findByTestId("MapView").click(660, 435);
    cy.findByTestId("MapView").click(660, 450);
    cy.findByTestId("MapView").click(660, 455);
    cy.findByTestId("MapView").click(500, 500);
    cy.findByTestId("drawtools-polygon").type("{esc}");
    cy.findByText(
      "The start position drawing allows a maximum of 6 individual points"
    ).should("exist");
  
    // erase and check warning again
    cy.findByTestId("drawtools-delete").click();
    cy.findByText("A start position drawing is required").should("exist");
  
    // draw another polygon
    cy.findByTestId("drawtools-polygon").click();
    cy.findByTestId("MapView").click(650, 400);
    cy.findByTestId("MapView").click(660, 450);
    cy.findByTestId("MapView").click(500, 500);
    cy.findByTestId("drawtools-polygon").type("{esc}");
  
    // Levels form
    cy.get("input[value=AT]").click();
    cy.get('[id="mui-component-select-level.unit"]').click();
    cy.get('[id="menu-level.unit"] li').then(($items) => {
      expect($items).to.have.length(2);
      expect($items).to.contain.text("FL");
      expect($items).to.contain.text("ft");
      $items.last().trigger("click");
    });
  
    // check rounding
    cy.get("input[name='level.value']").clear().type("10");
    cy.findByText("The minimum level in FL is 50").should("exist");
    cy.get("input[name='level.value']").clear().type("53");
    cy.findByText("Levels must be rounded to the nearest 5").should("exist");
    cy.get("input[name='level.value']").clear().type("700");
    cy.findByText("The maximum level in FL is 100").should("exist");
    cy.get("input[name='level.value']").clear().type("100");
  
    // Movement form
    cy.get("input[value=MOVEMENT]").click();
    cy.findByTestId("movement-movementDirection").click();
    cy.findByText("NE").click();
    cy.get('[id="mui-component-select-movementUnit"]').click();
    cy.get('[id="menu-movementUnit"] li').then(($items) => {
      expect($items).to.have.length(1);
      expect($items).to.contain.text("kt");
      $items.first().trigger("click");
    });
    // check rounding
    cy.get("input[name='movementSpeed']").clear().type("3");
    cy.findByText("The minimum level in kt is 5").should("exist");
    cy.get("input[name='movementSpeed']").clear().type("7");
    cy.findByText("Speed should be rounded to the nearest 5kt").should("exist");
    cy.get("input[name='movementSpeed']").clear().type("200");
    cy.findByText("The maximum level in kt is 150").should("exist");
    cy.get("input[name='movementSpeed']").clear().type("10");
  
    // Change form
    cy.get("input[value=WKN]").click();
  
    // Check the TAC message
    cy.findByText(
      "EHAA AMSTERDAM FIR TEST MOD ICE OBS " +
        "WI N5340 E00257 - N5436 E00519 - N5353 " +
        "E00534 - N5324 E00301 - N5340 E00257 " +
        "FL100 MOV NE 10KT WKN="
    ).should("exist");
  
    // publish
    cy.findByTestId("productform-dialog-publish").click();
    cy.findByTestId("confirmationDialog-confirm").click();
    // wait for the dialog to be closed
    cy.findByTestId("productform-dialog-publish").should("not.exist");
    // check that the SIGMET is published
    cy.findAllByText("published").should("exist");
  };
  
  describe("AIRMET Smoke Test", () => {
    const { app_url } = Cypress.env();
  
    it("should check the airmet form against the config", () => {
      cy.intercept("GET", "/workspacepreset?scope=user,system").as(
        "getWorkspace"
      );
      cy.intercept("POST", /\/airmet/).as("postAirmet");
  
      // open the AIRMET module from the workspace menu
      cy.findByText("Workspace menu").should("be.visible");
      cy.wait("@getWorkspace").then((interception) => {
        const result = interception.response.body;
        cy.findByText(`${result.length} results`);
      });
      cy.findAllByText("Airmet").first().click({ force: true });
  
      // request the airmet config - for now from FE
      // should be coming from BE soon
      cy.request({
        url: `${app_url}assets/knmi.airmetConfig.json`,
      }).then((response) => {
        expect(response.status).to.eq(200);
        const airmetConfig = response.body;
  
        // check that the loading bar appears and disappears after loading
        cy.findByTestId("productlist-loadingbar").should("exist");
        cy.findByTestId("productlist-loadingbar").should("not.exist");
  
        // get the lenght of Airmet list
        cy.get("body").then(($body) => {
          if ($body.find('[data-testid="productListItem"]').length) {
            // get length of list to verify later
            cy.findAllByTestId("productListItem").then(($elements) => {
              const listLength = $elements.length;
  
              makeAirmet(listLength, airmetConfig);
            });
          } else {
            // list is empty
            const listLength = 0;
            makeAirmet(listLength, airmetConfig);
          }
        });
  
        cy.findByTestId("close-btn").click();
        cy.findByTestId("sigmetAirmetComponent").should("not.exist");
      });
    });
  });
  