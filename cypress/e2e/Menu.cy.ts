beforeEach(() => {
  cy.login();
});

afterEach(() => {
  cy.logout();
});

describe("Menu Smoke Test", () => {
  it("should show the correct version", () => {
    cy.checkVersion();
  });
});
