beforeEach(() => {
  cy.login();
});

afterEach(() => {
  cy.logout();
});

describe("ViewPresets Smoke Test", () => {
  it("should open, save as and delete a viewpreset", () => {
    const now = new Date().toUTCString();
    cy.intercept("GET", "/viewpreset").as("getViewpresets");
    cy.intercept("GET", "/viewpreset?search=Radar").as("getViewpresetRadar");
    cy.intercept("GET", "viewpreset/singleRadar").as("loadPresetRadar");
    cy.intercept("POST", "/viewpreset").as("postViewpreset");
    cy.intercept("DELETE", /\/viewpreset/).as("deleteViewpreset");

    // close the workspace dialog
    cy.findByTestId("workspaceMenuBackdrop").findByTestId("closeBtn").click();
    cy.findByTestId("open-viewpresets").click();

    // verify that the viewpreset menu is open
    cy.findByText("View presets menu").should("be.visible");
    cy.wait("@getViewpresets").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // Make sure the Radar viewpreset comes in view
    cy.findByTestId("viewpresetMenuBackdrop").find("input").type("Radar");
    cy.wait("@getViewpresetRadar").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // Load preset Radar
    cy.findAllByText("Radar").first().click({ force: true });
    cy.wait("@loadPresetRadar");

    // Open layer manager
    cy.findByTestId("layerManagerButton").focus().click();
    // the enableButton occurs 4 times for each layer:
    cy.findAllByTestId("enableButton").should("have.length", 4);
    // Make a change
    // The enableButton shows up multiple times on the page
    // Used index to select the correct enableButton (thus eq(2))
    cy.findAllByTestId("enableButton").eq(2).click();
    // Save as a user preset
    cy.findAllByTestId("viewpreset-options-toolbutton").first().click();
    cy.findByText("Save as").click();
    cy.get("input[name='title']").clear().type(`Radar ${now}`);
    cy.findByTestId("confirmationDialog-confirm").click();
    cy.wait("@postViewpreset");
    // Delete a user preset
    cy.findByTestId("open-viewpresets").click();
    cy.findByTestId("viewpresetMenuBackdrop")
      .find("input")
      .type(`Radar ${now}`);

    cy.findAllByTestId("viewpreset-listDeleteButton").last().click();
    cy.findByTestId("confirmationDialog-confirm").click();
    cy.wait("@deleteViewpreset");
    cy.findByText(`0 results`);

    // Close the presets menu
    cy.findByTestId("open-viewpresets").click();
  });

  it("it should duplicate a viewpreset and filter viewpresets when clicking on filter chips", () => {
    cy.intercept("GET", "/viewpreset").as("getViewpresets");
    cy.intercept("GET", "/viewpreset?search=Radar").as("getViewpresetsRadar");
    cy.intercept("GET", "/viewpreset?scope=user&search=Radar").as(
      "getMyViewpresets"
    );
    cy.intercept("GET", "/viewpreset?scope=system&search=Radar").as(
      "getSystemViewpresets"
    );
    cy.intercept("POST", "/viewpreset").as("postViewpreset");
    cy.intercept("DELETE", /\/viewpreset/).as("deleteViewpreset");

    const now = new Date().toUTCString();

    // close the workspace dialog
    cy.findByTestId("workspaceMenuBackdrop").findByTestId("closeBtn").click();
    cy.findByTestId("open-viewpresets").click();

    // verify that the viewpreset menu is open
    cy.findByText("View presets menu").should("be.visible");
    cy.wait("@getViewpresets").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // Make sure the Radar viewpreset comes in view
    cy.findByTestId("viewpresetMenuBackdrop").find("input").type("Radar");
    cy.wait("@getViewpresetsRadar").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // duplicate a viewpreset to create a user viewpreset
    cy.findAllByTestId("viewpreset-listOptionsButton").first().click();
    cy.findByText("Duplicate").click();
    cy.get("input[name='title']").clear().type(`Radar filtered ${now}`);
    cy.findByText("Save").click();
    cy.wait("@postViewpreset");
    cy.wait("@getViewpresetsRadar").then((interception) => {
      const initialResult = interception.response.body;
      const initialLength = initialResult.length;

      cy.findByText(`${initialLength} results`);

      // filter My presets
      cy.findAllByText("My presets").last().click();
      // assert correct call being made
      cy.wait("@getMyViewpresets").then((interception) => {
        const result = interception.response.body;
        const myPresetsLength = result.length;
        cy.findByText(`${myPresetsLength} results`);

        // filter All presets
        cy.findAllByText("All").last().click();
        cy.wait("@getViewpresetsRadar").then((interception) => {
          const result = interception.response.body;
          cy.findByText(`${result.length} results`);
        });

        // filter System presets
        cy.findAllByText("System presets").last().click();
        // assert correct call being made
        cy.wait("@getSystemViewpresets").then((interception) => {
          const result = interception.response.body;
          cy.findByText(`${result.length} results`);
          cy.wrap(result).should(
            "have.length",
            initialLength - myPresetsLength
          );
        });
      });

      // filter All presets again
      cy.findAllByText("All").last().click();
      cy.wait("@getViewpresetsRadar").then((interception) => {
        const result = interception.response.body;
        cy.findByText(`${result.length} results`);
      });

      // clean up created viewpreset
      cy.findByTestId("viewpresetMenuBackdrop")
        .findByLabelText(`Radar filtered ${now}`)
        .next()
        .findByTestId("viewpreset-listDeleteButton")
        .click();
      // confirm to delete
      cy.findByText("Delete").click();
      cy.wait("@deleteViewpreset");
    });
  });

  it("it should search using search terms", () => {
    cy.intercept("GET", "/viewpreset").as("getViewpresets");
    cy.intercept("GET", "/viewpreset?scope=system").as("getSystemViewpresets");
    cy.intercept("GET", "/viewpreset?scope=system&search=harm").as(
      "getHarmSystemViewpresetsLowercase"
    );
    cy.intercept("GET", "/viewpreset?scope=system&search=HARM").as(
      "getHarmSystemViewpresetsUppercase"
    );
    cy.intercept("GET", "/viewpreset?scope=system&search=HRV,3h").as(
      "getHRV3hViewpresets"
    );
    cy.intercept("POST", "/viewpreset").as("postViewpreset");
    cy.intercept("DELETE", /\/viewpreset/).as("deleteViewpreset");

    const now = new Date().toUTCString();
    // close the workspace dialog
    cy.findByTestId("workspaceMenuBackdrop").findByTestId("closeBtn").click();
    cy.findByTestId("open-viewpresets").click();

    // verify that the viewpreset menu is open
    cy.findByText("View presets menu").should("be.visible");
    cy.wait("@getViewpresets").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // Get all system view presets
    cy.findAllByText("System presets").last().click();
    cy.wait("@getSystemViewpresets").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    cy.get("input").last().type("harm");
    cy.wait("@getHarmSystemViewpresetsLowercase").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // Do same search with all caps
    cy.get("input").last().clear();
    cy.wait("@getSystemViewpresets").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    cy.get("input").last().type("HARM");
    cy.wait("@getHarmSystemViewpresetsUppercase").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // Search by two keywords
    cy.get("input").last().clear();
    cy.wait("@getSystemViewpresets").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    cy.get("input").last().type("HRV 3h");
    cy.wait("@getHRV3hViewpresets").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // Switch back to all
    cy.get("input").last().clear();
    cy.wait("@getSystemViewpresets").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    cy.findAllByText("All").last().click();
    cy.wait("@getViewpresets").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // Duplicate existing preset and assign title
    cy.findAllByTestId("viewpreset-listOptionsButton").first().click();
    cy.findByText("Duplicate").click();
    cy.get("input[name='title']").clear().type(`New duplicate ${now}`);
    cy.findByText("Save").click();
    cy.wait("@postViewpreset");
    cy.wait("@getViewpresets").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // Search new preset
    cy.get("input").last().type(`New duplicate ${now}`);
    // Check there is only this one
    cy.findAllByTestId("viewpreset-selectListRow").should("have.length", 1);
    // Now, delete the new preset
    cy.findAllByTestId("viewpreset-listDeleteButton").first().click();
    // confirm to delete
    cy.findByText("Delete").click();
    cy.wait("@deleteViewpreset");
    cy.findByText(`0 results`);
  });
});
