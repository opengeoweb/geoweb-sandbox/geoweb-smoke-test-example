beforeEach(() => {
  cy.login();
});

afterEach(() => {
  cy.logout();
});

describe("TAF Smoke Test", () => {
  it("should publish a TAF", () => {
    cy.intercept("GET", "/workspacepreset?scope=user,system").as(
      "getWorkspace"
    );
    cy.intercept("GET", "/workspacepreset?scope=user,system&search=Taf").as(
      "getWorkspaceTaf"
    );

    cy.findByText("Workspace menu").should("be.visible");
    cy.wait("@getWorkspace").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // Search the TAF workspace
    cy.findAllByTestId("workspace-selectlist-connect")
      .find("input")
      .type("Taf");
    cy.wait("@getWorkspaceTaf").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // open the TAF module
    cy.findAllByText("Taf").first().click();

    // Check that the TAF module is shown
    cy.get("#tafmodule").should("be.visible");

    // Check that there are 6 or more upcoming TAFs
    cy.findByTestId("location-tabs")
      .get("li")
      .should("have.length.greaterThan", 5);

    cy.findByTestId("location-tabs")
      .get("li")
      .then(($locations) => {
        cy.get("[data-testid=taf-panel-active] p:first")
          .invoke("text")
          .then(($activeTafStatusText) => {
            // we can only do this part if there is a draft upcoming TAF or the active TAF is a New TAF
            if (
              $locations.find("[data-testid=status-draft]").length ||
              $activeTafStatusText === "new"
            ) {
              if ($locations.find("[data-testid=status-draft]").length) {
                // open the first draft TAF
                cy.wrap($locations.find("[data-testid=status-draft]"))
                  .first()
                  .click();
              }
              // switch to editor mode, take over if needed
              cy.get("[data-testid=taf-panel-active]").then(($activeTaf) => {
                if (!$activeTaf.find("[data-testid=avatar]").length) {
                  // no editor yet, we can safely switch to editor mode
                  cy.get("[data-testid=switchMode] input").click();
                } else if (
                  !$activeTaf.find("[data-testid=switchMode].Mui-checked")
                    .length
                ) {
                  // we are not the editor, take over editor mode
                  cy.get("[data-testid=switchMode] input").click();
                  // click confirm in the confirmation dialog
                  cy.findByTestId("confirmationDialog-confirm").click();
                }
              });

              // make sure we are in editor mode
              cy.findByTestId("switchMode").should("have.class", "Mui-checked");

              // open issues panel
              cy.findByTestId("issuesButton").click();

              cy.get("input[name='baseForecast.wind']").clear().type("12010");
              cy.get("input[name='baseForecast.visibility']").clear().type("c");
              // clear the other fields in case this already has values
              cy.get("input[name='baseForecast.weather.weather1']").clear();
              cy.get("input[name='baseForecast.weather.weather2']").clear();
              cy.get("input[name='baseForecast.weather.weather3']").clear();
              cy.get("input[name='baseForecast.cloud.cloud1']").clear();
              cy.get("input[name='baseForecast.cloud.cloud2']").clear();
              cy.get("input[name='baseForecast.cloud.cloud3']").clear();
              cy.get("input[name='baseForecast.cloud.cloud4']").clear();

              // Clear any changegroup data to make sure we can publish
              cy.get("[data-testid*='tafFormOptions']").each(
                ($element, index) => {
                  if (index === 0) return; // don't try to clear baseforecast
                  cy.wrap($element).click();
                  cy.findByText("Clear row").click();
                  cy.findByTestId("confirmationDialog-confirm").click();
                }
              );
              cy.findByTestId("publishtaf").click();
              cy.findByTestId("confirmationDialog-confirm").click();
              cy.findByTestId("publishtaf").should("not.exist");
              // the status should be published
              cy.findAllByText("published").should("exist");
              // make sure we are in viewer mode
              cy.findByTestId("switchMode").should(
                "not.have.class",
                "Mui-checked"
              );
            }
          });

        // we can only do this part if there is a published upcoming TAF
        if ($locations.find("[data-testid=status-upcoming]").length) {
          // amend the first published or amended TAF
          cy.wrap($locations.find("[data-testid=status-upcoming]"))
            .first()
            .click();
          // switch to editor mode, take over if needed
          cy.get("[data-testid=taf-panel-active]").then(($activeTaf) => {
            if (!$activeTaf.find("[data-testid=avatar]").length) {
              // no editor yet, we can safely switch to editor mode
              cy.get("[data-testid=switchMode] input").click();
            } else if (
              !$activeTaf.find("[data-testid=switchMode].Mui-checked").length
            ) {
              // we are not the editor, take over editor mode
              cy.get("[data-testid=switchMode] input").click();
              // click confirm in the confirmation dialog
              cy.findByTestId("confirmationDialog-confirm").click();
            }
          });
          // make sure we are in editor mode
          cy.findByTestId("switchMode").should("have.class", "Mui-checked");

          cy.findByTestId("amendtaf").click();
          cy.get("input[name=messageType]").should("have.value", "AMD");
          cy.get("input[name='baseForecast.visibility']").clear().type("1000");
          cy.get("input[name='baseForecast.weather.weather1']")
            .clear()
            .type("ra");
          cy.get("input[name='baseForecast.cloud.cloud1']")
            .clear()
            .type("FEW001");
          // clear any changegroup data to make sure we can publish
          cy.get("[data-testid*='tafFormOptions']").each(($element, index) => {
            if (index === 0) return; // don't try to clear baseforecast
            cy.wrap($element).click();
            cy.findByText("Clear row").click();
            cy.findByTestId("confirmationDialog-confirm").click();
          });
          cy.findByTestId("publishtaf").click();
          cy.findByTestId("confirmationDialog-confirm").click();
          cy.findByTestId("publishtaf").should("not.exist");
          // make sure we are in viewer mode
          cy.findByTestId("switchMode").should("not.have.class", "Mui-checked");
          // the status should be amended
          cy.findAllByText("published amendment").should("exist");
        }
      });
  });
});
