import { exec } from "child_process";
import exp = require("constants");

beforeEach(() => {
  cy.login();
});

afterEach(() => {
  cy.logout();
});

const makeSigmet = (listLength: number, sigmetConfig) => {
  // first make a draft SIGMET
  cy.findByTestId("productListCreateButton").click();
  cy.get("#mui-component-select-phenomenon").click();
  cy.findByText("Severe icing").click();
  cy.findByTestId("type").click();
  cy.findByText("Test").click();
  cy.findByTestId("productform-dialog-draft").click();
  // check that the new SIGMET dialog is closed
  cy.findByTestId("productform-dialog-draft").should("not.exist");
  // check that a new SIGMET is added to the list
  cy.findAllByTestId("productListItem").should("have.length", listLength + 1);

  // then update and publish the SIGMET
  cy.findAllByTestId("productListItem").first().click();
  cy.findByTestId("dialogTitle").should(
    "have.text",
    "SIGMET Severe icing - saved as draft"
  );

  // check the displayed firname with name from config
  expect(sigmetConfig).to.have.property("fir_areas");
  expect(sigmetConfig.fir_areas).to.have.property("EHAA");
  const firName = sigmetConfig["fir_areas"]["EHAA"]["fir_name"];
  cy.get("#mui-component-select-locationIndicatorATSR").should(
    "contain",
    `${firName}`
  );

  // fill in the form
  cy.get("input[value=OBS]").click();

  // enter a point and then delete, check warning message
  cy.findByTestId("drawtools-delete").click();
  cy.findByTestId("MapView").click(500, 500);
  cy.findByTestId("drawtools-point").type("{esc}");
  cy.findByTestId("drawtools-point").click();
  cy.findByText("A start position drawing is required").should("exist");

  // enter polygon drawing mode and draw polygon with many intersections
  cy.findByTestId("drawtools-polygon").click();
  cy.findByTestId("MapView").click(650, 400);
  cy.findByTestId("MapView").click(660, 450);
  cy.findByTestId("MapView").click(500, 450);
  cy.findByTestId("MapView").click(500, 200);
  cy.findByTestId("MapView").click(750, 300);
  cy.findByTestId("drawtools-polygon").type("{esc}");
  cy.findByText(
    "Intersection of the drawn polygon with the FIR-boundary has more than 6 individual points. Check if the TAC in the preview corresponds to the intended area."
  ).should("exist");

  // erase and check warning again
  cy.findByTestId("drawtools-delete").click();
  cy.findByText("A start position drawing is required").should("exist");

  // draw a polygon with too many vertices (more than 6) and check warning
  cy.findByTestId("drawtools-polygon").click();
  cy.findByTestId("MapView").click(650, 400);
  cy.findByTestId("MapView").click(655, 420);
  cy.findByTestId("MapView").click(660, 430);
  cy.findByTestId("MapView").click(660, 435);
  cy.findByTestId("MapView").click(660, 450);
  cy.findByTestId("MapView").click(660, 455);
  cy.findByTestId("MapView").click(500, 500);
  cy.findByTestId("drawtools-polygon").type("{esc}");
  cy.findByText(
    "The start position drawing allows a maximum of 6 individual points"
  ).should("exist");

  // erase and check warning again
  cy.findByTestId("drawtools-delete").click();
  cy.findByText("A start position drawing is required").should("exist");

  // draw another polygon
  cy.findByTestId("drawtools-polygon").click();
  cy.findByTestId("MapView").click(650, 400);
  cy.findByTestId("MapView").click(660, 450);
  cy.findByTestId("MapView").click(500, 500);
  cy.findByTestId("drawtools-polygon").type("{esc}");

  // Levels form
  cy.get("input[value=AT]").click();
  cy.get('[id="mui-component-select-level.unit"]').click();
  cy.get('[id="menu-level.unit"] li').then(($items) => {
    expect($items).to.have.length(2);
    expect($items).to.contain.text("FL");
    expect($items).to.contain.text("ft");
    $items.last().trigger("click");
  });
  // check rounding
  expect(sigmetConfig.fir_areas.EHAA).to.have.property("level_min");
  expect(sigmetConfig.fir_areas.EHAA.level_min).to.have.property("FL");
  expect(sigmetConfig.fir_areas.EHAA.level_min).to.have.property("FT");
  expect(sigmetConfig.fir_areas.EHAA.level_min.FL).to.equal(50);
  expect(sigmetConfig.fir_areas.EHAA.level_min.FT).to.equal(500);
  const min_FL = sigmetConfig.fir_areas.EHAA.level_min.FL;
  cy.get("input[name='level.value']").clear().type("10");
  cy.findByText(`The minimum level in FL is ${min_FL}`).should("exist");

  expect(sigmetConfig.fir_areas.EHAA).to.have.property("level_rounding_FL");
  expect(sigmetConfig.fir_areas.EHAA).to.have.property("level_rounding_FT");
  expect(sigmetConfig.fir_areas.EHAA.level_rounding_FL).to.equal(5);
  expect(sigmetConfig.fir_areas.EHAA.level_rounding_FT).to.equal(500);
  const round_FL = sigmetConfig.fir_areas.EHAA.level_rounding_FL;
  cy.get("input[name='level.value']").clear().type("53");
  cy.findByText(`Levels must be rounded to the nearest ${round_FL}`).should(
    "exist"
  );

  expect(sigmetConfig.fir_areas.EHAA).to.have.property("level_max");
  expect(sigmetConfig.fir_areas.EHAA.level_max).to.have.property("FL");
  expect(sigmetConfig.fir_areas.EHAA.level_max).to.have.property("FT");
  expect(sigmetConfig.fir_areas.EHAA.level_max.FL).to.equal(650);
  expect(sigmetConfig.fir_areas.EHAA.level_max.FT).to.equal(4500);
  const max_FL = sigmetConfig.fir_areas.EHAA.level_max.FL;
  cy.get("input[name='level.value']").clear().type("700");
  cy.findByText(`The maximum level in FL is ${max_FL}`).should("exist");

  cy.get("input[name='level.value']").clear().type("100");

  // Movement form
  cy.get("input[value=MOVEMENT]").click();
  cy.findByTestId("movement-movementDirection").click();
  cy.findByText("NE").click();
  cy.get('[id="mui-component-select-movementUnit"]').click();
  cy.get('[id="menu-movementUnit"] li').then(($items) => {
    expect($items).to.have.length(1);
    expect($items).to.contain.text("kt");
    $items.first().trigger("click");
  });
  // check rounding
  expect(sigmetConfig.fir_areas.EHAA).to.have.property("units");
  expect(sigmetConfig.fir_areas.EHAA.units).to.have.length(2);

  expect(sigmetConfig.fir_areas.EHAA).to.have.property("movement_min");
  expect(sigmetConfig.fir_areas.EHAA.movement_min).to.have.property("KT");
  expect(sigmetConfig.fir_areas.EHAA.movement_min).to.have.property("KMH");
  expect(sigmetConfig.fir_areas.EHAA.movement_min.KT).to.equal(5);
  expect(sigmetConfig.fir_areas.EHAA.movement_min.KMH).to.equal(10);
  const min_movement = sigmetConfig.fir_areas.EHAA.movement_min.KT;
  cy.get("input[name='movementSpeed']").clear().type("3");
  cy.findByText(`The minimum level in kt is ${min_movement}`).should("exist");

  expect(sigmetConfig.fir_areas.EHAA).to.have.property("movement_rounding_kt");
  expect(sigmetConfig.fir_areas.EHAA.movement_rounding_kt).to.equal(5);
  const round_movement = sigmetConfig.fir_areas.EHAA.movement_rounding_kt;
  cy.get("input[name='movementSpeed']").clear().type("7");
  cy.findByText(
    `Speed should be rounded to the nearest ${round_movement}kt`
  ).should("exist");

  expect(sigmetConfig.fir_areas.EHAA).to.have.property("movement_max");
  expect(sigmetConfig.fir_areas.EHAA.movement_max).to.have.property("KT");
  expect(sigmetConfig.fir_areas.EHAA.movement_max).to.have.property("KMH");
  expect(sigmetConfig.fir_areas.EHAA.movement_max.KT).to.equal(150);
  expect(sigmetConfig.fir_areas.EHAA.movement_max.KMH).to.equal(99);
  const max_movement = sigmetConfig.fir_areas.EHAA.movement_max.KT;
  cy.get("input[name='movementSpeed']").clear().type("200");
  cy.findByText(`The maximum level in kt is ${max_movement}`).should("exist");

  cy.get("input[name='movementSpeed']").clear().type("10");

  // Change form
  cy.get("input[value=WKN]").click();

  // Check the TAC message
  cy.findByText(
    "EHAA AMSTERDAM FIR TEST SEV ICE OBS " +
      "WI N5340 E00257 - N5436 E00519 - N5353 " +
      "E00534 - N5324 E00301 - N5340 E00257 " +
      "FL100 MOV NE 10KT WKN="
  ).should("exist");
  // publish
  cy.findByTestId("productform-dialog-publish").click();
  cy.findByTestId("confirmationDialog-confirm").click();
  // wait for the dialog to be closed
  cy.findByTestId("productform-dialog-publish").should("not.exist");
  // check that the SIGMET is published
  cy.findAllByText("published").should("exist");
};

describe("SIGMET Smoke Test", () => {
  const { app_url } = Cypress.env();

  it("should check the sigmet form against the config", () => {
    cy.intercept("GET", "/workspacepreset?scope=user,system").as(
      "getWorkspace"
    );
    cy.intercept("GET", "/workspacepreset?scope=user,system&search=Sigmet").as(
      "getWorkspaceSigmet"
    );
    cy.intercept("POST", /\/sigmet/).as("postSigmet");

    cy.findByText("Workspace menu").should("be.visible");
    cy.wait("@getWorkspace").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // Search the SIGMET workspace
    cy.findAllByTestId("workspace-selectlist-connect")
      .find("input")
      .type("Sigmet");
    cy.wait("@getWorkspaceSigmet").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // open the SIGMET module from the workspace menu
    cy.findAllByText("Sigmet").first().click({ force: true });

    // request the sigmet config - for now from FE
    // should be coming from BE soon
    cy.request({
      url: `${app_url}assets/knmi.sigmetConfig.json`,
    }).then((response) => {
      expect(response.status).to.eq(200);
      const sigmetConfig = response.body;

      // check that the loading bar appears and disappears after loading
      cy.findByTestId("productlist-loadingbar").should("exist");
      cy.findByTestId("productlist-loadingbar").should("not.exist");

      // get the lenght of Sigmet list
      cy.get("body").then(($body) => {
        if ($body.find('[data-testid="productListItem"]').length) {
          // get length of list to verify later
          cy.findAllByTestId("productListItem").then(($elements) => {
            const listLength = $elements.length;

            makeSigmet(listLength, sigmetConfig);
          });
        } else {
          // list is empty
          const listLength = 0;
          makeSigmet(listLength, sigmetConfig);
        }
      });

      cy.findByTestId("close-btn").click();
      cy.findByTestId("sigmetAirmetComponent").should("not.exist");
    });
  });
});
