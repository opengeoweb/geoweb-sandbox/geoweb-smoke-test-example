beforeEach(() => {
  cy.login();
});

afterEach(() => {
  cy.logout();
});

describe("LayerManager Smoke Test", () => {
  it("should duplicate and remove a layer", () => {
    cy.intercept("GET", "/workspacepreset?scope=user,system").as(
      "getWorkspace"
    );
    cy.intercept("GET", "/workspacepreset/radarSingleMapScreenConfig").as(
      "getRadar"
    );

    // make sure workspace menu is open and list is loaded
    cy.findByText("Workspace menu").should("be.visible");
    cy.wait("@getWorkspace").then((interception) => {
      const result = interception.response.body;
      cy.findByText(`${result.length} results`);
    });

    // select preset radar preset
    cy.findAllByLabelText("Radar").first().click({ force: true });
    cy.findByTestId("workspace-options-toolbutton").should("contain", "Radar");
    cy.wait("@getRadar");
    cy.findByTestId("layerManagerButton").click();
    // the enableButton occurs 4 times for each layer:
    cy.findAllByTestId("enableButton").then(($elements) => {
      const listLength = $elements.length;
      // listlength is derived from the number of enableButtons
      // as four enableButtons show up per single layer
      // and the legend only shows the single layer that is displayed
      // the legend length should be 1/4th of the listLength
      const legendLength = listLength * 0.25;

      // check that legend is open
      cy.findByTestId("open-Legend").click();
      // listlength is derived from the number of enableButtons
      // as four enableButtons show up per single layer
      // and the legend only shows the single layer that is displayed
      // the legend length should be 1/4th of the listLength
      cy.findAllByTestId("legend").should("have.length", legendLength);

      // duplicate a layer
      cy.findAllByTestId("openMenuButton").first().click();
      cy.findByText("Duplicate layer").click();
      // after duplicating a layer, number of enableButtons and legends
      // should be duplicated as well
      cy.findAllByTestId("enableButton").should("have.length", listLength * 2);
      cy.findAllByTestId("legend").should("have.length", legendLength * 2);

      // remove a layer
      cy.findAllByTestId("deleteButton").last().click(); // or eq(1);
      cy.findAllByTestId("enableButton").should("have.length", listLength);
      // the length of the legend should be 1/2 listlength
      cy.findAllByTestId("legend").should("have.length", legendLength);

      // close the layermanager
      cy.findByTestId("layerManagerWindow").findByTestId("closeBtn").click();
      cy.findByTestId("layerManagerWindow").should("not.exist");
    });
  });
});
