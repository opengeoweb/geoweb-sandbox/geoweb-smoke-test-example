# GeoWeb Smoke Test Example

This repo contains example code to set up a smoke test for a deployed GeoWeb application using Cypress. No backend requests are mocked, so the application should have working backends for aviation, presets, warnings etc.

## Run locally

To run this smoketest locally:

1. `npm ci`
2. In `cypress.config.ts` you can update the test settings: on which environment to run and the user credentials to use (don't commit any changes here). Dependent on which environment you are executing the test, you should pay attention to fill in the following fields correctly:
   - `app_url`: URL to the deployed GeoWeb application, make sure to finish the URL with `/`
   - `version`: the version that is displayed in the user menu
     You can also set these environment variables commandline, for example for the credentials: `export cypress_username=your.username` and `export cypress_password=yourPassword`.
3. `npm run test` This will run the cypress tests, any failed tests will add a screenshot in /cypress/screenshots/ and if video is enabled also a video in /cypress/videos/.
4. Alternatively you can use `npm run cypress`. This opens the Cypress GUI and let's you choose which tests to run, which browser, debug etc.

New tests can be added in [`/cypress/e2e`](cypress/e2e).

## Run in CI/CD

To run in a gitlab pipeline a job can be set up. See example below. The username and password should be saved as CI/CD variables.

```
smoke-test-tst:
  stage: smoke-test-tst
  image:
    name: cypress/included:13.2.0 # the image should match the installed cypress version
    entrypoint: [""]
  script:
    - CYPRESS_INSTALL_BINARY=0 npm ci
    - npm run test -- -e username=${SMOKETEST_USERNAME},password=${SMOKETEST_PASSWORD_TST},app_url=https://geoweb-tst.geoweb.dev.knmi.cloud/,version=master
  allow_failure: false
  artifacts:
    when: on_failure
    paths:
      - ./cypress/screenshots/
    expire_in: 3 months
  only:
    - master
```

## Run typecheck

`npm run typecheck`
